package com.config360.test;

import java.util.Locale;

import com.netappsid.testmanager.ConfigurationTestManager.ConfigurationTestManagerVerboseLevel;
import com.netappsid.testmanager.StoresTestManager;

public class MainTest
{
	public static void main(String[] args)
	{
		StoresTestManager tests = new StoresTestManager(Locale.CANADA);
		// tests.recordTest(ConfigurationTestManagerVerboseLevel.ALL);
		tests.playTest(ConfigurationTestManagerVerboseLevel.ALL);
	}
}
